# README #

### What is this repository for? ###

Run commands on remote servers

The tool takes a list of servers and logs into them to run a list of commands. Both of these lists are given as filenames. A credentials file can be used to authenticate with devices, one can be created by using my encrypted user tool [https://bitbucket.org/schizoid90/encrypted_user](Link URL). If no credentials file exists then it will ask a for a password to log into the routers and the username will be assumed as being your current username. To use a different user either create the credentials file or switch to that user.

It will iterate over the list of hosts in the given file, attempt to SSH in and run the commands in the order given in the commands list. If SSH fails then the script falls back to telnet.

-L will specify a directory for storing command output. -t can be used to set a custom timeout for the pexpect module.

There is also -E that will pass the "enable" command to whatever you're connecting to before sending other commands, this is used to elevate privileges on some switches/routers. To acheive the same on Linux servers, you should include "sudo" in all commands that need elevated privs

The below example takes a hidden credentials file, list of hosts and commands from in the same directory as the script. Uses -E to elevate privileges and points log files to a directory in your home directory.

    python -m remote -c .credentials -H hosts.list -C cmds.list -E -L /home/me/logs

The logs are output in one file per device.

### How do I get set up? ###

    git clone git@bitbucket.org:schizoid90/remotescript.git

Install requirements

    pip install -r requirements.txt

Full list of options with

    python -m remote -h

    -h, --help            show this help message and exit
    -c OUTPUT_CREDS, --creds OUTPUT_CREDS
                        specify .credentials file
    -t OUTPUT_TIMEOUT, --timeout OUTPUT_TIMEOUT
                        Specify timeout in secs
    -H OUTPUT_HOST, --hosts OUTPUT_HOST
                        specify hosts file
    -C OUTPUT_CMDS, --cmds OUTPUT_CMDS
                        specify commands file
    --enable, -E          pass the enable command when logged in
    -L OUTPUT_LOG, --logdir OUTPUT_LOG
                        select a directory to store the command output
    -I OUTPUT_KEY, --key OUTPUT_KEY
                        pass your ssh public key file
    --forwarding, -A      if you're logged in using ssh key forwarding this will
                        use your existing key
    --notelnet, -T        Do not fall back to Telnet if SSH fails
    --allowroot           Allow root logins, NOT RECOMMENDED