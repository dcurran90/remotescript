import os
import sys
import pexpect
from logging import *
from outcome import *
from remote import *

ssh_newkey = 'Are you sure you want to continue connecting'

"""[summary]
Connect to the hosts with SSH
[description]
Connect to each host using SSH.
Based on what pexpect receives, run other modules:
If SSH is refused then run telnet()
If the server cannot be connected then run fail()
If connection is successful then run sshsuccess()
"""
def ssh(hip, logdir, U, P, enable, CMDS, key, forward, T, timeout):
    print('Running host - %s' % (hip,))
    try:
        if not os.path.exists(logdir):
            try:
                os.mkdir(logdir)
            except Exception as e:
                print('Cannot crearte directory %s - %s' % (logidr, e))
        hlogfile = '%s/%s.log' % (logdir, str(hip),)
        basicConfig(filename=hlogfile,
            level=DEBUG,
            format='%(asctime)s - %(levelname)s - %(message)s'
        )
        debug('=============== %s ===============' % (hip,))
        if key:
            connect = pexpect.spawn('ssh -i %s %s@%s' % (key, U, str(hip)))
        elif forward:
            connect = pexpect.spawn('ssh %s@%s' % (U, str(hip),))
        else:
            connect = pexpect.spawn('ssh %s:@%s' % (U, str(hip),))
        connect.timeout = timeout
        i = connect.expect([
            'Connection refused',
            'Name or service not known',
            'No route to host',
            'Host key verification failed',
            ssh_newkey,
            '[P|p]assword',
            'Connection timed out',
            pexpect.TIMEOUT,
            'Connection closed by remote host',
            'Welcome',
            ])
        if i==0 or i==8:
            if not T:
                telnet(hip, U, P, CMDS, timeout)
            else:
                print('')
        if i==1 or i==2 or i==3 or i==6 or i==7:
            ### TO DO ###
            ### Get a meaningful error ###
            error = 'There has been an error'
            fail(hip, error,)
        if i==4 or i==5 or i==9:
            sshsuccess(i, hip, U, P, enable, CMDS, key, connect)
    except Exception as e:
        debug(e)
        print(e)
    debug(sys.exc_info())

"""[summary]
Telnet in the event of SSH failure
[description]
Connect with telnet in the even of SSH not being refused
If -T is used then no telnet connection will be attempted
"""
def telnet(hip, U, P, CMDS, timeout):
    debug('Failed login %s, attempting telnet' % (hip,))
    try:
        connectt = pexpect.spawn('telnet %s 22' % (str(hip),))
        connectt.timeout = timeout
    except pexpect.TIMEOUT:
        debug('%s timed out' % (hip,))
        pass
    t = connectt.expect([
        'Connection refused',
        pexpect.EOF,
        'Username:',
        'Escape character is'
        ], timeout=10)
    if t==0 or t==1:
        fail()
    if t==2 or t==3:
        telsuccess(connectt, U, P, hip, CMDS)

if __name__ == '__main__':
    print('Cannot run this directly')
    sys.exit()
