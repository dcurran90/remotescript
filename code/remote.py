#!/usr/bin/python
import os
import sys
import getpass
import pickle
import argparse
import multiprocessing
from logging import *
from sagecipher import *
from time import sleep
from connect import *

threads = []

"""
[summary]
Kick off script for running commands on remote machines
[description]
This script will run a log into a list of hosts
and run given commands
for more information use:
python remote.py -h
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog = "python remote.py",
        description="Use -c for credentials -H for host file and -C for commands"
    )
    parser.add_argument(
        "-c", "--creds",
        dest="output_creds",
        help="specify .credentials file",
    )
    parser.add_argument(
        "-t", "--timeout",
        dest="output_timeout",
        help="Specify timeout in secs",
    )
    parser.add_argument(
        "-H", "--hosts",
        dest="output_host",
        help="specify hosts file",
    )
    parser.add_argument(
        "-C", "--cmds",
        dest="output_cmds",
        help="specify commands file",
    )
    parser.add_argument(
        "--enable", "-E",
        action="store_true",
        help="pass the enable command when logged in",
    )
    parser.add_argument(
        "-L", "--logdir",
        dest="output_log",
        help="select a directory to store the command output",
    )
    parser.add_argument(
        "-I", "--key",
        dest="output_key",
        help="pass your ssh public key file",
    )
    parser.add_argument(
        "--forwarding", "-A",
        action="store_true",
        help="""if you're logged in using ssh key forwarding
        this will use your existing key"""
    )
    parser.add_argument(
        "--notelnet", "-T",
        action="store_true",
        help="Do not fall back to Telnet if SSH fails"
    )
    parser.add_argument(
        "--allowroot",
        action="store_true",
        help="Allow root logins, NOT RECOMMENDED"
    )
    args = parser.parse_args()

    """[summary]
    How you will authenticate with the remote device
    [description]
    This section decides how we'll authenticate
    -c - Using an encrypted crentials file
    -I - Using a given SSH public key
    -A - Using the key carried forwaring with -A ssh option
    If none of these options then the script will take your username
    and ask for a password
    """
    key = ""
    forward = False

    if args.output_creds:
        if args.forwarding:
            if args.output_key:
                print("Cannot use -I with -A")
                sys.exit()
            print("Cannot use -A with -c")
            sys.exit()
        if args.output_key:
            if args.forwarding:
                print("Cannot use -A with -I")
                sys.exit()
            print("Cannot use -I with -c")
            sys.exit()

        cred_file = args.output_creds
        if cred_file:
            creds = pickle.loads(decrypt_string(open(cred_file, "r").read()))
            U = creds[0]
            P = creds[1]
        else:
            print("Can't find credentials %s, exiting" % (args.output_creds,))
    else:
        U = getpass.getuser()
        P = getpass.getpass(prompt="Password: ")
        if args.output_key:
            if args.forward:
                print("Cannot use -A with -I")
                sys.exit()
            key = args.output_key
        if args.forwarding:
            if args.output_key:
                print("Cannot use -I with -A")
                sys.exit()
            forward = True

    """[summary]
    Hosts file and commands file
    [description]
    Pass the hosts and commands lists to variables
    -H for hosts file
    -C for commands file
    """
    if args.output_host:
        hfile = args.output_host
    else:
        print("Must pass a hosts file")
    if args.output_cmds:
        cfile = args.output_cmds
    else:
        print("Must pass a commands file")

    """[summary]
    Escalate privileges with enable command
    [description]
    If logging into switch or router, pass enable command in order to escalate privileges
    -E will pass the enable command
    """
    if args.enable:
        enable = True
    else:
        enable = False

    """[summary]
    Location of log directory
    [description]
    Each host will log to a file under this directory
    if the directory does not exist, it will be created
    -L to set log directory
    """
    if args.output_log:
        logdir = args.output_log
    else:
        logdir = "%s/logs" % (os.getcwd(),)

    """[summary]
    Set the no telnet flag
    [description]
    -T to disable telnet
    """
    if args.notelnet:
        T = True
    else:
        T = False

    if args.output_timeout:
        timeout = int(args.output_timeout)
    else:
        timeout = 60

    """[summary]
    Don't allow root logins
    [description]
    Don't allow root logins as this poses a security risk
    unless --allow-root is specified
    """
    if U == "root":
        if args.allowroot:
            print("""
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!Root logins are not recommended due to the potential security issues!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            """)
            confirm = raw_input("Are you sure you want to continue [yN]: ")
            if confirm == "y":
                print("On your head be it")
            else:
                sys.exit()
        else:
            print("""
Cannot use root user
please use your own user and escalate privileges the correct way
Use -E to run enable
Or include sudo in your commands file""")
            sys.exit()

    """[summary]
    Turn the hosts and commands files into lists
    [description]
    Turn the hosts and commands files into lists so that they can be iterated over
    """
    HOSTS = open(hfile).readlines()
    CMDS = open(cfile).readlines()

    """[summary]
    Attempt to connect to the devices
    [description]
    Connects to each device simultaneously
    """
    for H in HOSTS:
        hip = H.replace("\n", "")
        t = multiprocessing.Process(name="ssh", target=ssh, args=(hip, logdir, U, P, enable, CMDS, key, forward, T, timeout))
        threads.append(t)
        t.start()
