import pexpect
import sys
from logging import *
from remote import *
from connect import *

"""[summary]
Log erorrs
[description]
If connecting to the server fails, log the error
"""
def fail(hip, error):
    debug("Could not connect to host %s, %s" % (hip, error,))

"""[summary]
Run commands on the device
[description]
This loops over the list of commands and runs them on the device
"""
def sendcommands(hip, U, P, enable, CMDS, key, connect):
    s = connect.expect([
        "Permission denied",
        "Welcome",
        "Invalid credentials",
        "@",
        "Last login"
        ])
    if s==0 or s==2:
        error = "Failed to login"
        fail(hip, error,)
    if s==1 or s==3 or s==4:
        debug("%s Login OK" % (hip,))
        if enable:
            connect.sendline("enable")
            en = connect.expect([
                "password",
                "Password",
                "#"
                ])
            if en==0 or en==1:
                connect.sendline(P)
            else:
                pass
        for cmd in CMDS:
            connect.sendline(cmd)
        connect.sendline("logout")
        debug(connect.read())

"""[summary]
Run the commands on the server after successful login
[description]
When logged in to the server, run sendcommands()
"""
def sshsuccess(i, hip, U, P, enable, CMDS, key, connect):
    if i==4:
        connect.sendline("yes")
        sleep(3)
        y = connect.expect([
            "[P|p]assword",
            "verification failed"
            ])
        if y==0:
            connect.sendline(P)
            sendcommands(hip, U, P, enable, CMDS, key, connect)
        else:
            debug(y)
    elif i==5:
        if key:
            print("Key not authorised for access on %s, sending password" % (hip,))
        connect.sendline(P)
        sendcommands(hip, U, P, enable, CMDS, key, connect)
    elif i==9:
        sendcommands(hip, U, P, enable, CMDS, key, connect)

"""[summary]
Run commands on the box via telnet
[description]
On successful login run the commands
"""
def telsuccess(connectt, U, P, hip, CMDS):
    connectt.sendline(U)
    connectt.expect("[P|p]assword:")
    connectt.sendline(P)
    connectt.expect([
        "#",
        ">"
        ])
    debug("connected to %s" % (hip,))
    if enable:
        connectt.sendline("enable")
        en = connectt.expect([
            "[P|p]assword",
            "#"
            ])
        if en==0:
            connectt.sendline(P)
        else:
            pass
    for cmd in CMDS:
        connectt.sendline(cmd)
    # log out of our telnet connections
    connectt.sendline("exit")
    debug(connectt.read())

if __name__ == "__main__":
    print("Cannot run this directly")